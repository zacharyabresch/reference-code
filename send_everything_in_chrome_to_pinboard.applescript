-- If you're daring (or don't want to hard code your credentials) you can experiment with the lines below
-- set theUsername to the text returned of (display dialog "Username?" default answer "")
-- set thePassword to the text returned of (display dialog "Password?" default answer "" with hidden answer)
-- set theToken to the text returned of (display dialog "API Token?" default answer "")

set theToken to "yourusername:yourpinboardtoken"

tell application "Google Chrome"
	set theWindow to the front window
	set theTabs to theWindow's tabs
	repeat with eachTab in theTabs
		set rawTitle to title of eachTab
		set replaceIndex to offset of "'" in rawTitle
		-- replace apostrophes
		set replacedTitle to (text 1 thru (replaceIndex - 1) of rawTitle) & "" & (text (replaceIndex + 1) thru -1 of rawTitle)
		set rawURL to URL of eachTab
		set rawTags to "send_everything"
		try
			set theTitle to do shell script "php -r 'echo urlencode(\"" & replacedTitle & "\");'"
			set theURL to do shell script "php -r 'echo urlencode(\"" & rawURL & "\");'"
			set theTags to do shell script "php -r 'echo urlencode(\"" & rawTags & "\");'"
			set thePinboardResult to do shell script "curl \"https://api.pinboard.in/v1/posts/add?auth_token=" & theToken & "&url=" & theURL & "&description=" & theTitle & "&tags=" & theTags & "&toread=yes\""
			close eachTab
		on error
			display dialog "An error occurred!"
		end try
	end repeat
	
	if theWindow exists then
		-- close theWindow
	end if
end tell