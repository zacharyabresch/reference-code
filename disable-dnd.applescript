on hazelProcessFile(theFile)
	
	do shell script "defaults write ~/Library/Preferences/ByHost/com.apple.notificationcenterui.*.plist doNotDisturb -boolean true"
	set theDate to quoted form of (do shell script "date +\"%Y-%m-%d %I:%M:%S +0000\"")
	do shell script "defaults write ~/Library/Preferences/ByHost/com.apple.notificationcenterui.*.plist doNotDisturbDate -date " & theDate
	do shell script "killall NotificationCenter"
	
end hazelProcessFile